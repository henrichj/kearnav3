import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardViewComponent } from './core/views/dashboard-view/dashboard-view.component';
import { LoginViewComponent } from './core/views/login-view/login-view.component';
import { AuthGuardService } from './guards/auth/auth-guard.service';
import { PageNotFoundComponent } from './core/components/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardViewComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'login',
    component: LoginViewComponent
  },
  {
    path: 'task',
    loadChildren: './modules/task/task.module#TaskModule',
    canActivate: [AuthGuardService]
  },

  {
    path: '**',
    component: PageNotFoundComponent,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
