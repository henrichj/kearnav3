export interface ITaskTemplate {
  title: string;
  priority: string;
  description: string;
}
