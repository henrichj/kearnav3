export interface ITask {
  title: string;
  priority: string;
  description: string;
}
