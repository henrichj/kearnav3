import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardHeaderComponent } from './components/dashboard-header/dashboard-header.component';

@NgModule({
  imports: [CommonModule],
  declarations: [DashboardHeaderComponent],
  exports: [DashboardHeaderComponent]
})
export class SharedModule {}
