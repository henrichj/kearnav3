import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'kae-dashboard-header',
  templateUrl: './dashboard-header.component.html',
  styleUrls: ['./dashboard-header.component.scss']
})
export class DashboardHeaderComponent implements OnInit {

  @Input() subHeader: string;
  @Input() mainHeader: string;

  constructor() { }

  ngOnInit() {
  }

}
