import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'kae-task-template-card',
  templateUrl: './task-template-card.component.html',
  styleUrls: ['./task-template-card.component.scss']
})
export class TaskTemplateCardComponent implements OnInit {
  @Input() taskTemplates;
  @Input() taskTitle: string;

  @Output() templateClicked = new EventEmitter();

  templates: any = [
    {
      title: 'Titel der Vorlage 1',
      priority: 1,
      description: 'Beschreibung der Vorlage 1'
    },
    {
      title: 'Titel der Vorlage 2',
      priority: 3,
      description: 'Beschreibung der Vorlage 2'
    },
    {
      title: 'Titel der Vorlage 3',
      priority: 1,
      description: 'Beschreibung der Vorlage 3'
    },
    {
      title: 'Titel der Vorlage 4',
      priority: 2,
      description: 'Beschreibung der Vorlage 4'
    },
    {
      title: 'Titel der Vorlage 5',
      priority: 3,
      description: 'Beschreibung der Vorlage 5'
    }
  ];

  ngOnInit() {}

  onTemplateClicked(template: any) {
    this.templateClicked.emit(template);
  }
}
