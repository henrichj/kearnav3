import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardViewComponent } from './views/dashboard-view/dashboard-view.component';
import { TaskRoutingModule } from './task-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AddTaskViewComponent } from './views/addtaskview/addtaskview.component';
import { FormsModule } from '@angular/forms';
import { TaskTemplateCardComponent } from './components/task-template-card/task-template-card.component';
import { StoreModule } from '@ngrx/store';
import { reducers } from 'src/app/modules/task/store';
import { EffectsModule } from '@ngrx/effects';
import { TaskEffects } from './store/effects/task.effects';

@NgModule({
  imports: [
    CommonModule,
    TaskRoutingModule,
    SharedModule,
    FormsModule,
    StoreModule.forFeature('taskState', reducers),
    EffectsModule.forFeature([TaskEffects])
  ],
  declarations: [
    DashboardViewComponent,
    AddTaskViewComponent,
    TaskTemplateCardComponent
  ]
})
export class TaskModule {}
