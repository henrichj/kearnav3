import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { ITaskTemplate } from '../../../../models/interfaces/ITaskTemplate';
import * as fromTask from '../../store/index';
import * as taskActions from '../../store/actions/task.actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'kae-addtaskview',
  templateUrl: './addtaskview.component.html',
  styleUrls: ['./addtaskview.component.scss']
})
export class AddTaskViewComponent implements OnInit {
  template: any = {};

  taskTemplates$: Observable<ITaskTemplate[]>;

  constructor(private _store: Store<fromTask.TaskState>) {
    this._store.dispatch({ type: taskActions.LOADTASKTEMPLATE });
  }

  ngOnInit() {
    this.taskTemplates$ = this._store.select(fromTask.getTasksTemplates);
  }

  onSubmit(newTaskForm: NgForm) {
    console.log(newTaskForm.value);
  }

  templateClicked(template: any) {
    this.template = template;
  }
}
