import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTaskViewComponent } from './addtaskview.component';

describe('AddtaskviewComponent', () => {
  let component: AddTaskViewComponent;
  let fixture: ComponentFixture<AddTaskViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTaskViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTaskViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
