import * as taskActions from '../actions/task.actions';
import { ITask } from '../../../../models/interfaces/ITask';
import { ITaskTemplate } from '../../../../models/interfaces/ITaskTemplate';

export interface TaskState {
  allTask: ITask[];
  taskTemplates: ITaskTemplate[];
}

const initialTaskState: TaskState = {
  allTask: [],
  taskTemplates: []
};

export function taskReducer(
  state: TaskState = initialTaskState,
  action: taskActions.TaskAction
): TaskState {
  switch (action.type) {
    case taskActions.LOADTASKS:
      return {
        allTask: [],
        taskTemplates: state.taskTemplates
      };
    case taskActions.LOADTASKS_SUCCESSFULL:
      return {
        allTask: [
          {
            title: 'TaskTitle',
            priority: '1',
            description: 'Task Description 1'
          }
        ],
        taskTemplates: state.taskTemplates
      };
    case taskActions.LOADTASKS_FAILED:
      return {
        allTask: [],
        taskTemplates: state.taskTemplates
      };
    case taskActions.LOADTASKTEMPLATE:
      return {
        allTask: state.allTask,
        taskTemplates: state.taskTemplates
      };
    case taskActions.LOADTASKTEMPLATE_SUCCESSFUL:
      return {
        allTask: state.allTask,
        taskTemplates: action.payload
      };
    case taskActions.LOADTASKTEMPLATE_FAILED:
      return {
        allTask: state.allTask,
        taskTemplates: state.taskTemplates
      };
    default:
      return state;
  }
}
