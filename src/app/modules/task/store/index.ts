import * as fromTask from './reducer/task.reducer';
import * as fromRoot from '../../../core/store/index';

import { createFeatureSelector, createSelector } from '@ngrx/store';

export interface TaskState {
  taskState: fromTask.TaskState;
}

export interface State extends fromRoot.State {
  taskState: TaskState;
}

export const reducers = {
  taskState: fromTask.taskReducer
};

export const getTaskState = createFeatureSelector<TaskState>('taskState');
export const getTasks = createSelector(
  getTaskState,
  state => state.taskState.allTask
);
export const getTasksTemplates = createSelector(
  getTaskState,
  state => state.taskState.taskTemplates
);
