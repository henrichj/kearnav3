import { Action } from '@ngrx/store';

export const LOADTASKS = '[Task] Load all Tasks';
export const LOADTASKS_SUCCESSFULL = '[Task] Load all Tasks successfull';
export const LOADTASKS_FAILED = '[Task] Load all Tasks failed';

export const LOADTASKTEMPLATE = '[Tasktemplate] Load all Tasks';
export const LOADTASKTEMPLATE_SUCCESSFUL =
  '[Tasktemplate] Load all Taskstemplates successfull';
export const LOADTASKTEMPLATE_FAILED =
  '[Tasktemplate] Load all Taskstemplates failed';

export class LoadTask implements Action {
  readonly type = LOADTASKS;
}

export class LoadTaskSuccess implements Action {
  readonly type = LOADTASKS_SUCCESSFULL;

  constructor(public payload: any) { }
}

export class LoadTaskFailed implements Action {
  readonly type = LOADTASKS_FAILED;
}

export class LoadTaskTemplates implements Action {
  readonly type = LOADTASKTEMPLATE;
}

export class LoadTaskTemplatesSuccess implements Action {
  readonly type = LOADTASKTEMPLATE_SUCCESSFUL;

  constructor(public payload: any) { }
}

export class LoadTaskTemplatesFailed implements Action {
  readonly type = LOADTASKTEMPLATE_FAILED;
}

export type TaskAction =
  LoadTask
  | LoadTaskSuccess
  | LoadTaskFailed
  | LoadTaskTemplates
  | LoadTaskTemplatesSuccess
  | LoadTaskTemplatesFailed;
