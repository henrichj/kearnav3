import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Action } from '@ngrx/store';

import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import * as taskActions from '../actions/task.actions';
import { TaskService } from '../../../../services/task/task-service.service';

@Injectable()
export class TaskEffects {
    @Effect()
    getTaskTemplates$: Observable<Action> = this.actions$.pipe(
        ofType(taskActions.LOADTASKTEMPLATE),
        mergeMap(() =>
            this._taskService.getTaskTemplates().pipe(
                map((taskData: any) => {
                    return new taskActions.LoadTaskTemplatesSuccess(taskData);
                }),
                catchError(() => of({ type: 'SELECT_ALL_FAIL' }))
            )
        )
    );

    constructor(private actions$: Actions, private _taskService: TaskService) { }
}
