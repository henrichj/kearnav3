import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardViewComponent } from './views/dashboard-view/dashboard-view.component';
import { AddTaskViewComponent } from './views/addtaskview/addtaskview.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardViewComponent
  },
  {
    path: 'add',
    component: AddTaskViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskRoutingModule {}
