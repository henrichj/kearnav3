import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { AuthService } from '../../core/services/auth/auth.service';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(private _authService: AuthService, private _router: Router) { }

  canActivate() {
    return this._authService.isLoggedIn.pipe(
      map(userLoggedIn => {
        if (!userLoggedIn) {
          this._router.navigateByUrl('/login');
          return userLoggedIn;
        } else {
          return userLoggedIn;
        }
      })
    );
  }
}
