import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './core/components/navbar/navbar.component';
import { SidebarComponent } from './core/components/sidebar/sidebar.component';
import { DashboardViewComponent } from './core/views/dashboard-view/dashboard-view.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { LoginViewComponent } from './core/views/login-view/login-view.component';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './core/store/effects/auth.effects';
import { AuthService } from './core/services/auth/auth.service';
import { StorageService } from './core/services/storage/storage.service';
import { PageNotFoundComponent } from './core/components/page-not-found/page-not-found.component';
import { SharedModule } from './modules/shared/shared.module';
import { SidebarmenuitemComponent } from './core/components/sidebar/components/sidebarmenuitem/sidebarmenuitem.component';
import { SidebarmenusubnavComponent } from './core/components/sidebar/components/sidebarmenusubnav/sidebarmenusubnav.component';
import { reducers, metaReducers } from './core/store';
import { HttpClientModule } from '@angular/common/http';
import { TaskService } from './services/task/task-service.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    DashboardViewComponent,
    LoginViewComponent,
    PageNotFoundComponent,
    SidebarmenuitemComponent,
    SidebarmenusubnavComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production // Restrict extension to log-only mode
    }),
    SharedModule,
    EffectsModule.forRoot([AuthEffects])
  ],
  providers: [StorageService, AuthService, TaskService],
  bootstrap: [AppComponent]
})
export class AppModule { }
