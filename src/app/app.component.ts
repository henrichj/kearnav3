import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './core/services/auth/auth.service';

@Component({
  selector: 'kae-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isAuthenticated$: Observable<boolean>;

  constructor(private _authService: AuthService) {
    this.isAuthenticated$ = this._authService.isLoggedIn;
  }
}
