import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'kae-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  menuItems = [
    {
      caption: 'Dashboards',
      baseRoute: '',
      subNavItems: [
        {
          caption: 'Home',
          route: ''
        }
      ]
    },
    {
      caption: 'Aufgaben',
      baseRoute: 'task',
      subNavItems: [
        {
          caption: 'Übersicht',
          route: ''
        },
        {
          caption: 'Hinzufügen',
          route: 'add'
        }
      ]
    }
  ];

  constructor() { }

  ngOnInit() { }
}
