import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarmenuitemComponent } from './sidebarmenuitem.component';

describe('SidebarmenuitemComponent', () => {
  let component: SidebarmenuitemComponent;
  let fixture: ComponentFixture<SidebarmenuitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarmenuitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarmenuitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
