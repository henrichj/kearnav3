import { Component, OnInit, Input } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'kae-sidebarmenuitem',
  templateUrl: './sidebarmenuitem.component.html',
  styleUrls: ['./sidebarmenuitem.component.scss'],
  animations: [
    trigger('menuActive', [
      state(
        'false',
        style({
          display: 'none'
        })
      ),
      state(
        'true',
        style({
          display: 'block'
        })
      ),
      transition('false => true', animate('1s ease-in')),
      transition('true => false', animate('1ms ease-out'))
    ])
  ]
})
export class SidebarmenuitemComponent implements OnInit {
  @Input() menuItem;

  isActive = true;

  constructor() {}

  ngOnInit() {}

  onCaptionClicked() {
    this.isActive = !this.isActive;
    console.log(this.isActive);
  }
}
