import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarmenusubnavComponent } from './sidebarmenusubnav.component';

describe('SidebarmenusubnavComponent', () => {
  let component: SidebarmenusubnavComponent;
  let fixture: ComponentFixture<SidebarmenusubnavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarmenusubnavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarmenusubnavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
