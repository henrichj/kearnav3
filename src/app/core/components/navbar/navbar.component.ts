import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { Observable } from 'rxjs';

import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'kae-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  currentUser$: Observable<any>;

  constructor(private _authService: AuthService) {
    this.currentUser$ = this._authService.currentUser;
  }

  ngOnInit() {}

  logout() {
    this._authService.logout();
  }
}
