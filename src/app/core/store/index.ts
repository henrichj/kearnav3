import {
    ActionReducer,
    ActionReducerMap,
    createFeatureSelector,
    createSelector,
    MetaReducer
} from '@ngrx/store';

import * as authReducers from '../store/reducers/auth.reducer';
import { environment } from '../../../environments/environment';

export interface State {
    auth: authReducers.AuthState;
}

export const reducers: ActionReducerMap<State, any> = {
    auth: authReducers.reducer
};

export function logger(
    reducer: ActionReducer<State>
): ActionReducer<State> {
    return function (state: State, action: any): State {
        console.log('state', state);
        console.log('action', action);

        return reducer(state, action);
    };
}

// TODO: Add storeFreeze as metaReducer
// (throws some errors in the console at the moment, because a reducers is modifiying the state somehwere)
export const metaReducers: MetaReducer<State>[] = !environment.production
    ? [logger]
    : [];
