import { Action } from '@ngrx/store';

export const AUTHENTICATE = '[Auth] Authenticate';
export const AUTHENTICATION_SUCCESSFULL = '[Auth] Authentication successfull';
export const AUTHENTICATION_FAILED = '[Auth] Authentication failed';

export const LOGOUT = '[Auth] Logout';
export const LOGOUT_SUCCESSFULL = '[Auth] Logout successfull';
export const LOGOUT_FAILED = '[Auth] Logout failed';

export class Authenticate implements Action {
  readonly type = AUTHENTICATE;

  constructor(public payload: any) {}
}

export class AuthenticationSuccess implements Action {
  readonly type = AUTHENTICATION_SUCCESSFULL;

  constructor(public payload: any) {}
}

export class AuthenticationFailed implements Action {
  readonly type = AUTHENTICATION_FAILED;
}

export class Logout implements Action {
  readonly type = LOGOUT;
}

export class LogoutSuccess implements Action {
  readonly type = LOGOUT_SUCCESSFULL;
}

export class LogoutFailed implements Action {
  readonly type = LOGOUT_FAILED;
}

export type AuthAction =
  | Authenticate
  | AuthenticationSuccess
  | AuthenticationFailed
  | Logout
  | LogoutSuccess
  | LogoutFailed;
