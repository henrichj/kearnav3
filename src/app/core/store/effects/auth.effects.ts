import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Action } from '@ngrx/store';

import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import * as authenticationActions from '../actions/auth.actions';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffects {
  @Effect()
  authenticate$: Observable<Action> = this.actions$.pipe(
    ofType(authenticationActions.AUTHENTICATE),
    map((credentials: any) => {
      const user = {
        userName: 'Johannes',
        email: credentials.payload.email
      };
      this._authService.setLoginState(true);
      this._authService.setUser(user);
      this._router.navigateByUrl('/dashboard');
      return {
        type: authenticationActions.AUTHENTICATION_SUCCESSFULL,
        payload: user
      };
    }),
    catchError(() => of({ type: authenticationActions.AUTHENTICATION_FAILED }))
  );

  @Effect()
  logout$: Observable<Action> = this.actions$.pipe(
    ofType(authenticationActions.LOGOUT),
    map(() => {
      this._authService.logout();
      this._router.navigateByUrl('/login');
      return {
        type: authenticationActions.LOGOUT_SUCCESSFULL
      };
    }),
    catchError(() => of({ type: authenticationActions.AUTHENTICATION_FAILED }))
  );

  constructor(
    private actions$: Actions,
    private _authService: AuthService,
    private _router: Router
  ) { }
}
