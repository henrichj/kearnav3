import * as authenticationActions from '../actions/auth.actions';

export interface AuthState {
  isAuthenticated: boolean;
  currentUser: any;
}

const initialState: AuthState = {
  isAuthenticated: false,
  currentUser: {}
};

export function reducer(
  state = initialState,
  action: authenticationActions.AuthAction
): AuthState {
  switch (action.type) {
    case authenticationActions.AUTHENTICATE:
      return {
        isAuthenticated: false,
        currentUser: {}
      };

    case authenticationActions.AUTHENTICATION_SUCCESSFULL:
      return {
        isAuthenticated: true,
        currentUser: action.payload
      };

    case authenticationActions.AUTHENTICATION_FAILED:
      return {
        isAuthenticated: false,
        currentUser: {}
      };
    case authenticationActions.LOGOUT:
      return {
        isAuthenticated: true,
        currentUser: {}
      };

    case authenticationActions.LOGOUT_SUCCESSFULL:
      return {
        isAuthenticated: false,
        currentUser: {}
      };

    case authenticationActions.LOGOUT_FAILED:
      return {
        isAuthenticated: false,
        currentUser: {}
      };
    default:
      return state;
  }
}
