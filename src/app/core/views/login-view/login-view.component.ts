import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AuthState } from '../../store/reducers/auth.reducer';
import * as authenticationActions from '../../store/actions/auth.actions';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'kae-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.scss']
})
export class LoginViewComponent implements OnInit {
  submitted: boolean;

  constructor(private _store: Store<AuthState>) {}

  ngOnInit() {}

  onSubmit(loginForm: NgForm) {
    this.submitted = true;
    this._store.dispatch({
      type: authenticationActions.AUTHENTICATE,
      payload: loginForm.value
    });
  }
}
