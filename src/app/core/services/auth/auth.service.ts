import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { StorageService } from '../storage/storage.service';
import { Store } from '@ngrx/store';

import { JwtHelperService } from '@auth0/angular-jwt';

import { IUser } from '../../../models/interfaces/IUser';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn$ = new BehaviorSubject<boolean>(false);
  private currentUser$ = new BehaviorSubject<IUser>({
    userName: '',
    email: ''
  });

  constructor(private _storageService: StorageService) {}

  public Authenticate(username: string, password: string) {
    // Call the Webservice and return the status
  }

  get isLoggedIn(): Observable<boolean> {
    const token = this._storageService.getValue('auth_token');

    if (token === null) {
      this.setLoginState(false);
      return this.loggedIn$.asObservable();
    }

    const jwtHelper = new JwtHelperService();
    const decodedToken = jwtHelper.decodeToken(token);

    // TODO: Check more values here
    const tokenExpired = !jwtHelper.isTokenExpired(token);
    this.loggedIn$.next(!tokenExpired);
    return this.loggedIn$.asObservable();
  }

  public setLoginState(authFlag: boolean) {
    this.loggedIn$.next(authFlag);
  }

  public setUser(authToken: any) {
    this._storageService.setValue(
      'auth_token',
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' +
        'eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ikp' +
        'vaGFubmVzIEhlbnJpY2giLCJlbWFpbCI6IkpvaGFu' +
        'bmVzLkhlbnJpY2hAZ21haWwuY29tIiwiaWF0IjoxNTE2MjM5MDIyMn0.ohD6l_xlGynkIzj0WVH0y1IWgOBLcB9qbrB_wE0ECFM'
    );
  }

  get currentUser() {
    const userToken = this._storageService.getValue('auth_token');

    const jwtHelper = new JwtHelperService();
    const decodedToken = jwtHelper.decodeToken(userToken);

    const currentUser: IUser = {
      userName: decodedToken.name,
      email: decodedToken.email
    };

    this.currentUser$.next(currentUser);
    return this.currentUser$.asObservable();
  }

  logout() {
    this._storageService.remove('auth_token');
    this.loggedIn$.next(false);
  }
}
